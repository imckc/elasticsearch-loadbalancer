script "install_nginx" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    yum -y install nginx
    /etc/init.d/nginx start
  EOH
end

script "nginx_conf" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    echo '
    server {
        large_client_header_buffers 32 8k;
        listen                80;

        server_name           rx9-search.mobileads.com;
        #access_log            /var/log/nginx/kibana4.mobileads.com.access.log;

         location / {
            index  index.html  index.htm;
            add_header 'Access-Control-Allow-Origin' 'kibana-rx9.mobileads.com';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';

            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            proxy_pass http://10.10.2.61:9200/;
            auth_basic "Restricted";
            auth_basic_user_file /etc/nginx/htpasswd;
        }
    }
    ' > /etc/nginx/conf.d/search-lb.conf
    EOH
  not_if { File.exist?("/etc/nginx/conf.d/search-lb.conf") }
end

script "create_htpasswd" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    echo '
    ckc:vTiyNWh.ZytTg
    ckinleng:Btvny9cEOVUtA
    boonsheng:$apr1$k0i0n7I4$o2YbDH.n//VTluZAN6jRU.
    sharonhoo:lem.CWyYlsJ2U
    alvin:7Am0RN78umjKY
    ninjoe:$apr1$zVKb0mUn$okieFpa4AN8kTSlWYrhOr1
    csm:$apr1$q8MYu8FL$TzJnP/eJO3NjLZ7KUQaLZ1
    el@stics3arch:$apr1$2/f8RuZr$l2BKAUVTcpDFtZowlJChs0
    ' > /etc/nginx/htpasswd
    EOH
  not_if { File.exist?("/etc/nginx/htpasswd") }
end

# services
execute "start-nginx" do
  command "nginx start"
  action :nothing
end