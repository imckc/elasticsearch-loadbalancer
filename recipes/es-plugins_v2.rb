script "install_plugin_license" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install license
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/license") }
end

script "install_plugin_cloud_aws" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install cloud-aws
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/cloud-aws") }
end

script "install_plugin_marvel" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install marvel-agent
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/marvel-agent") }
end

script "install_plugin_shield" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install shield
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/shield") }
end


script "install_plugin_watcher" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install watcher
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/watcher") }
end

