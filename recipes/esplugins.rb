# Install ElasticSearch AWS S3 plugin
script "install_plugin_cloud_aws" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install cloud-aws
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/cloud-aws") }
end

script "install_plugin_license" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install license
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/license") }
end

script "install_plugin_marvel" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install marvel-agent
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/marvel-agent") }
end

# Install elasticfence
script "install_plugin_elasticfence" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin install https://github.com/elasticfence/elasticsearch-http-user-auth/raw/2.4/jar/elasticfence-2.4.0-SNAPSHOT.zip
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/elasticfence") }
end